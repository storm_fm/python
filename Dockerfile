############################################################
# Dockerfile to build dev App
# Based on
############################################################

# Set the base image
FROM ubuntu:latest

# File Author / Maintainer
LABEL image.author="storm_fm@mail.ru"

#add addon
RUN apt-get update && apt-get install -y apache2 \
    libapache2-mod-wsgi-py3 \
    python3-pip \   
    && apt-get clean \
    && apt-get autoremove \
    && rm -rf /var/lib/apt/lists/*

#Copy config to apache
COPY ./apache2.conf      /etc/apache2/apache2.conf
COPY ./000-default.conf  /etc/apache2/sites-enabled/000-default.conf
COPY ./index.py /var/www/index.py

#enable mod
RUN a2enmod headers
RUN a2enmod cgi


# LINK apache config to docker logs.
RUN ln -sf /proc/self/fd/1 /var/log/apache2/access.log && \
    ln -sf /proc/self/fd/1 /var/log/apache2/error.log

EXPOSE 80

WORKDIR /var/www/

CMD  /usr/sbin/apache2ctl -D FOREGROUND
