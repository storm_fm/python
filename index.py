#!/usr/bin/python3
import subprocess
import json
import sys


class ReqHandler:
   
    def get_code(self, err_txt='None'):
  
        if err_txt != 'None':
            self.data = dict(status="error",
                             error=err_txt)
     
        else:
            try:
               print("")
            except Exception as err:
                self.print_err("Internal error")


    def __init__(self):
        self.status = 200

  
    def get_request(self):
        self.req = ""
        a = ''
        for line in sys.stdin:
            a += line
        try:
            self.post_req = json.loads(a)
            self.req = self.post_req["request"]
            self.header_token = self.post_req["authorization-token"]

        except Exception as err:
            self.print_err("Wrong Request")

   
        self.status = 200

  
    def read_conf(self):
        try:
            with open('webconf.json') as json_file:
                data = json.load(json_file)                
                if self.header_token not in data["Tokens"]:
                    self.print_err("Authorization failed", 401)
        except Exception as err:
            self.print_err("Wrong config file")

   
    def print_err(self, err, status=500):
        self.get_code(err)
        self.status = status
        self.print_out()
        exit()

    def print_out(self):
        print("Content-Type: text/plain")
        print("Status:"+str(self.status))
        print("")
        print(json.dumps(self.data))


pr = ReqHandler()
pr.get_request()
pr.get_code("None")
pr.print_out()